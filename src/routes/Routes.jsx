import { lazy } from "react";
import PrivateRoute from "./PrivateRoute";
import { createBrowserRouter } from "react-router-dom";
import Project from "../views/Project/Project";

import Layout from "../layout/Layout";
import AddProject from "../views/Project/AddProject";
import AddTask from "../views/Task/AddTask";
import ViewProject from "../views/Project/ViewProject";
import Login from "../views/Login/Login";
import SignIn from "../views/SignIn/SignIn";

const routes = createBrowserRouter([
    {
        path: '/',
        element: <Layout />,
        children: [
            {
                path: '/',
                element: <PrivateRoute />, // Protect the route
                children: [
                    {
                        path: '/',
                        element: <Project />, // Render Project when authenticated
                    },
                    {
                        path: '/add-project',
                        element: <AddProject />, // Render Project when authenticated
                    },
                    {
                        path: '/edit-project/:id',
                        element: <AddProject />, // Render Project when authenticated
                    },
                    {
                        path: '/add-task',
                        element: <AddTask />, // Render Project when authenticated
                    },
                    {
                        path: '/view-project/:id',
                        element: <ViewProject />, // Render Project when authenticated
                    },
                ],
            },
        ],
    },
    {
        path: '/login',
        element: <Login />,
    },
    {
        path: '/sign-in',
        element: <SignIn />,
    },
    // Uncomment and define your AccessDenied component if needed
    // {
    //     path: '/access-denied',
    //     element: <AccessDenied />,
    // },
]);

export default routes;