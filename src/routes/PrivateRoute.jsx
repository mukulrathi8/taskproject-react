import { Navigate, Outlet, useNavigate } from 'react-router-dom';
import Layout from '../layout/Layout';
import { ErrorBoundary } from 'react-error-boundary'
import tokenService from '../apiservices/token.service';

const PrivateRoute = ({ children }) => {
    const auth =    tokenService.getLocalAccessToken();
    return auth ? <Outlet /> : <Navigate to="/login"  replace />;
};

export default PrivateRoute;