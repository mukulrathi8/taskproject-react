import React from "react";
import {toast} from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";

const toastConfig = {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "colored",
}

const Toast = ({ message,type}) => {
    switch (type) {
        case'success':
            return toast.success(message, toastConfig);
        case 'error':
            return toast.error(message, toastConfig);
        case 'info':
            return toast.info(message, toastConfig);
        default:
            toast(message, toastConfig)
    }

    return null
}

export default Toast;