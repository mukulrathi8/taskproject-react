import { Suspense } from 'react'
import { RouterProvider } from "react-router-dom";
import route from "./routes/Routes";
import {ToastContainer} from 'react-toastify';
function App() {


  return (
    <>
      <ToastContainer autoClose={1500}/>
       <Suspense
        fallback={
          <div className="loader-container ">
            <div className="spinner"></div>
          </div>
        }
      >
        <RouterProvider router={route} />
      </Suspense>

    </>
  )
}

export default App
