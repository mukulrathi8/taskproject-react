import { Suspense, useEffect, useState } from "react";
import dataService from "../../apiservices/data.service";
import { Link } from "react-router-dom";

const Project = () => {
    const [data, setData] = useState([]);

    useEffect(() => {
        dataService.listProject().then((res) => {
            if (res.data.flag) {
                setData(res.data.data)
            }
        })
    }, [])
    return <>
        <section>
            <div className="">

                <section>
                    <div className="mx-auto max-w-screen-xl px-1 py-2 sm:px-6 sm:py-12 lg:px-8">
                        <header className="text-left">
                            <h2 className="text-xl font-bold text-gray-900 sm:text-3xl">Project List</h2>
                        </header>

                        <div className="overflow-x-auto mt-10">
                            <table className="min-w-full divide-y-2 divide-gray-200 bg-white text-sm">
                                <thead className="ltr:text-left rtl:text-right">
                                    <tr>
                                        <th className="whitespace-nowrap px-4 py-2 font-medium text-gray-900">Name</th>
                                        <th className="whitespace-nowrap px-4 py-2 font-medium text-gray-900 text-center">Description</th>
                                        <th className="px-4 py-2"></th>
                                    </tr>
                                </thead>

                                <tbody className="divide-y divide-gray-200">
                                    <Suspense fallback="loading">
                                        {data && data.length > 0 ? <>
                                            {data.map((item, index) => {

                                                return <tr className="mx-auto mt-4 max-w-md text-gray-500" key={index}>
                                                    <td className="text-center whitespace-nowrap px-4 py-2 font-medium text-gray-900">{item.name}</td>
                                                    <td className="text-center whitespace-nowrap px-4 py-2 text-gray-700 max-w-xs overflow-hidden">
                                                        <span className="break-words block max-w-xs truncate">{item.description}</span>
                                                    </td>  <td>
                                                        <span className="inline-flex -space-x-px overflow-hidden rounded-md border bg-white shadow-sm">
                                                            <Link
                                                                to={`edit-project/${item.row_guid}`}
                                                                className="inline-block px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50 focus:relative"
                                                            >
                                                                Edit
                                                            </Link>

                                                            <Link
                                                                to={`add-task`}
                                                                state={{ project_id: item.id }}
                                                                className="inline-block px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50 focus:relative"
                                                            >
                                                                Add Task
                                                            </Link>

                                                            <Link
                                                                to={`view-project/${item.row_guid}`}
                                                                className="inline-block px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50 focus:relative"
                                                            >
                                                                View Project
                                                            </Link>
                                                        </span>
                                                    </td>
                                                </tr>


                                            })}


                                        </> :
                                            <div className="text-center  px-4 py-2 font-medium text-gray-900">No Project Added </div>


                                        }
                                    </Suspense>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </section>
    </>
}


export default Project;