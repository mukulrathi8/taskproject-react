import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import dataService from "../../apiservices/data.service";
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import _ from "lodash"
const ViewProject = () => {
    const { id } = useParams();
    const [data, setData] = useState([]);

    if (id) {
        useEffect(() => {
            dataService.viewProjectData(id).then((res) => {
                console.log(res.data.data)
                if (res.data.flag) {
                    setData(res.data.data)
                }
            })
        }, [])
    }

    const taskList = _.groupBy(data[0]?.project_tasks, 'status');
    return (
        <>
            <div className="mx-auto max-w-screen-xl px-2 py-5 sm:px-6 lg:px-8">



                <div className="flow-root">
                    <dl className="-my-3 divide-y divide-gray-100 text-sm">
                        <div className="grid grid-cols-1 gap-1 py-3 even:bg-gray-50 sm:grid-cols-3 sm:gap-4">
                            <dt className="font-medium text-gray-900">Name</dt>
                            <dd className="text-gray-700 sm:col-span-2">{data[0]?.name}
                                <div class="bg-gray-100 flex flex-col gap-4 h-10 mt-4 place-content-center sm:items-center t text-xs w-32">
                                    <Link
                                        to='/add-task'
                                        state={{ project_id: data[0]?.id }}
                                        className=""
                                    >
                                        Add Tasks
                                    </Link>
                                </div>


                            </dd>
                        </div>

                        <div className="grid py-3 sm:gap-4 sm:grid-cols-3">
                            <dt className="font-medium text-gray-900">Description</dt>
                            <dd className="text-gray-700 sm:col-span-2">{data[0]?.description}</dd>
                        </div>

                        <div className="grid grid-cols-1 gap-1 py-3 even:bg-gray-50 sm:grid-cols-3 sm:gap-4">
                            <dt className="font-medium text-gray-900">Created At</dt>
                            <dd className="text-gray-700 sm:col-span-2">{data[0]?.created_at}</dd>
                        </div>



                    </dl>
                </div>
                <span className="flex items-center mx-auto max-w-screen-xl px-4 py-8 sm:px-6 sm:py-12 lg:px-8">
                    <span className="h-px flex-1 bg-black"></span>
                    <span className="shrink-0 px-6">Task List</span>
                    <span className="h-px flex-1 bg-black"></span>
                </span>
                <div className="grid grid-cols-1 gap-4 lg:grid-cols-4 lg:gap-8 py-4">
                    {Object.keys(taskList).map(columnName => (
                        <div key={columnName} className="bg-gray-100 rounded-lg p-4 w-64">
                            <h2 className="text-lg font-bold mb-4">{columnName.toUpperCase()}</h2>
                            {taskList[columnName].map(task => (
                                <div key={task.id} className="bg-white rounded-lg p-2 mb-2 shadow">
                                    <h3 className="font-semibold">{task.name}</h3>
                                    <p className="text-sm text-gray-500">{task.description}</p>
                                    <p className="text-sm text-gray-500">Due Date: {task.due_date}</p>
                                    <p className="text-sm text-gray-500">Priority:


                                        {task.priority == '1' ? (
                                            <span className="whitespace-nowrap rounded-full bg-purple-100 px-2.5 py-0.5 text-sm text-purple-700">
                                                High
                                            </span>
                                        ) : task.priority == '2' ? (
                                            <span className="whitespace-nowrap rounded-full bg-purple-100 px-2.5 py-0.5 text-sm text-green-700">
                                                Medium
                                            </span>
                                        ) : (
                                            <span className="whitespace-nowrap rounded-full bg-purple-100 px-2.5 py-0.5 text-sm text-red-700">
                                                Low
                                            </span>
                                        )}


                                    </p>
                                </div>
                            ))}
                        </div>
                    ))}
                </div>
            </div>

        </>
    )
}

export default ViewProject