import { useFormik } from "formik";
import { useNavigate, Link, useParams } from "react-router-dom";

import * as Yup from 'yup';
import dataService from "../../apiservices/data.service";
import Toast from "../../components/Toast.jsx";
import { useEffect, useState } from "react";

const AddProject = () => {
    const navigate = useNavigate();
    const [data, setData] = useState([]);
    const { id } = useParams();
    if (id) {
        useEffect(() => {
            dataService.listProject(id).then((res) => {
                if (res.data.flag) {
                    setData(res.data.data)

                }
            })
        }, [])
    }
    const formik = useFormik({
        initialValues: {
            name: data[0]?.name,
            description: data[0]?.description,
        },
        enableReinitialize: data.length > 0 ? true : false,
        validationSchema: Yup.object({
            name: Yup.string()
                .min(10, 'Min be 10 characters or less')
                .required('Required'),
            description: Yup.string().required('Required'),
        }),
        onSubmit: (values, { setErrors }) => {

            if (data.length > 0) {
                dataService.updateProject(id,values).then((res) => {
                    const responseFlag = res.data.flag;
                    if (responseFlag) {
                        Toast({ message: res.data.message, type: 'success' });
                   
                    } else {
                        if (Object.keys(res.data.data.error).length > 0 && res.data.code == 422) {
                            const serverErrors = res.data.data.error;
                            setErrors({
                                name: serverErrors.name ? serverErrors.name.join(' ') : '',
                                description: serverErrors.description ? serverErrors.description?.join(' ') : '',
                            });
                        }
                    }

                })
            } else {
                dataService.addProject(values).then((res) => {
                    const responseFlag = res.data.flag;
                    if (responseFlag) {
                        formik.resetForm();
                        Toast({ message: res.data.message, type: 'success' });
                        

                    } else {
                        if (Object.keys(res.data.data.error).length > 0 && res.data.code == 422) {
                            const serverErrors = res.data.data.error;
                            setErrors({
                                name: serverErrors.name ? serverErrors.name.join(' ') : '',
                                description: serverErrors.description ? serverErrors.description?.join(' ') : '',
                            });
                        }
                    }

                })
            }


        },
    });

    return <>
        <div className="mx-auto max-w-screen-xl px-2 py-5 sm:px-6 lg:px-8">
            <div className="mx-auto max-w-lg">
                <h1 className="text-center text-2xl font-bold text-indigo-600 sm:text-3xl">Get started today</h1>

                <p className="mx-auto mt-4 max-w-md text-center text-gray-500">
                   Add Project
                </p>

                <form action="#" className="mb-0 mt-6 space-y-4 rounded-lg p-4 shadow-lg sm:p-6 lg:p-8" onSubmit={formik.handleSubmit}>

                    <div>
                        <label htmlFor="name" className="block text-sm font-medium text-gray-700">Project Name</label>

                        <div className="relative">
                            <input
                                type="text"
                                name="name"
                                id="name"
                                className="mt-2 w-full rounded-lg border-gray-200 align-top shadow-sm sm:text-sm"
                                placeholder="Enter Name"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.name}
                            />
                            {formik.touched.name && formik.errors.name ? (
                                <div className="text-red-400 font-size text-sm py-1">{formik.errors.name}</div>
                            ) : null}

                        </div>
                    </div>

                    <div>
                        <label htmlFor="OrderNotes" className="block text-sm font-medium text-gray-700"> Description </label>

                        <textarea
                            id="OrderNotes"
                            className="mt-2 w-full rounded-lg border-gray-200 align-top shadow-sm sm:text-sm"
                            rows="4"
                            name="description"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.description}
                            placeholder="Enter any description..."
                        ></textarea>
                        {formik.touched.description && formik.errors.description ? (
                            <div className="text-red-400 font-size text-sm py-1">{formik.errors.description}</div>
                        ) : null}
                    </div>

                    <button
                        type="submit"
                        className="block w-full rounded-lg bg-indigo-600 px-5 py-3 text-sm font-medium text-white"
                    >
                        {data.length > 0 ?'Edit' : 'Add'}
                    </button>


                </form>
            </div>
        </div>
    </>
}
export default AddProject