import { useFormik } from "formik";
import { useNavigate, Link, useParams, useLocation } from "react-router-dom";

import * as Yup from 'yup';
import dataService from "../../apiservices/data.service.js";
import Toast from "../../components/Toast.jsx";
import { useEffect, useState } from "react";

const AddTask = () => {
    const navigate = useNavigate();
    const [data, setData] = useState([]);
    let { state } = useLocation();

    const formik = useFormik({
    // Initialize Formik Field Values
        initialValues: {
            name: '',
            description: '',
            project_id: state.project_id,
            priority: '',
            status: '',
            due_date: ''

        },
        // This is for showing preselected value
        enableReinitialize: data.length > 0 ? true : false,

         // Validation formik
        validationSchema: Yup.object({
            name: Yup.string()
                .min(10, 'Must be at least 10 characters')
                .required('Required'),
            description: Yup.string().required('Required'),
            priority: Yup.string().required('Required'),
            status: Yup.string().required('Required'),
            due_date: Yup.date().required('Required')
        }),
        onSubmit: (values, { setErrors }) => {

            if (data.length > 0) {
                dataService.updateProject(id, values).then((res) => {
                    const responseFlag = res.data.flag;
                    if (responseFlag) {
                        Toast({ message: res.data.message, type: 'success' });

                    } else {
                        if (Object.keys(res.data.data.error).length > 0 && res.data.code == 422) {
                            const serverErrors = res.data.data.error;
                            setErrors({
                                name: serverErrors.name ? serverErrors.name.join(' ') : '',
                                description: serverErrors.description ? serverErrors.description?.join(' ') : '',
                            });
                        }
                    }

                })
            } else {
                dataService.addTask(values).then((res) => {
                    const responseFlag = res.data.flag;
                    if (responseFlag) {
                        Toast({ message: res.data.message, type: 'success' });
                        formik.resetForm();

                    } else {
                        if (Object.keys(res.data.data.error).length > 0 && res.data.code == 422) {
                            const serverErrors = res.data.data.error;
                              // Managing Server side Error for field
                            setErrors({
                                name: serverErrors.name ? serverErrors.name.join(' ') : '',
                                description: serverErrors.description ? serverErrors.description?.join(' ') : '',
                                due_date: serverErrors.due_date ? serverErrors.due_date?.join(' ') : '',
                                status: serverErrors.status ? serverErrors.status?.join(' ') : '',
                                priority: serverErrors.priority ? serverErrors.priority?.join(' ') : '',

                            });
                        }
                    }

                })
            }


        },
    });

    return <>
        <div className="mx-auto max-w-screen-xl px-2 py-5 sm:px-6 lg:px-8">
            <div className="mx-auto max-w-lg">
                <h1 className="text-center text-2xl font-bold text-indigo-600 sm:text-3xl">Get started today</h1>

                <p className="mx-auto mt-4 max-w-md text-center text-gray-500">
                    Add Task
                </p>

                <form action="#" className="mb-0 mt-6 space-y-4 rounded-lg p-4 shadow-lg sm:p-6 lg:p-8" onSubmit={formik.handleSubmit}>

                    <div>
                        <label htmlFor="name" className="block text-sm font-medium text-gray-700">Project Name</label>

                        <div className="relative">
                            <input
                                type="text"
                                name="name"
                                id="name"
                                className="mt-2 w-full rounded-lg border-gray-200 align-top shadow-sm sm:text-sm"
                                placeholder="Enter Name"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.name}
                            />
                            {formik.touched.name && formik.errors.name ? (
                                <div className="text-red-400 font-size text-sm py-1">{formik.errors.name}</div>
                            ) : null}

                        </div>
                    </div>

                    <div>
                        <label htmlFor="OrderNotes" className="block text-sm font-medium text-gray-700"> Description </label>

                        <textarea
                            id="OrderNotes"
                            className="mt-2 w-full rounded-lg border-gray-200 align-top shadow-sm sm:text-sm"
                            rows="4"
                            name="description"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.description}
                            placeholder="Enter any description..."
                        ></textarea>
                        {formik.touched.description && formik.errors.description ? (
                            <div className="text-red-400 font-size text-sm py-1">{formik.errors.description}</div>
                        ) : null}
                    </div>


                    <div>
                        <label htmlFor="HeadlineAct" className="block text-sm font-medium text-gray-900"> Priority </label>

                        <select
                            name="priority"
                            id="HeadlineAct"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.priority}
                            className="mt-1.5 w-full rounded-lg border-gray-300 text-gray-700 sm:text-sm"
                        >
                            <option value="">Please select</option>
                            <option value="1">High</option>
                            <option value="2">Medium</option>
                            <option value="3">Low</option>

                        </select>

                        {formik.touched.priority && formik.errors.priority ? (
                            <div className="text-red-400 font-size text-sm py-1">{formik.errors.priority}</div>
                        ) : null}
                    </div>

                    <div>
                        <label htmlFor="due_date" className="block text-sm font-medium text-gray-700">Due Date</label>

                        <div className="relative">
                            <input
                                type="date"
                                name="due_date"
                                id="due_date"
                                min={new Date().toISOString().split('T')[0]}
                                className="mt-2 w-full rounded-lg border-gray-200 align-top shadow-sm sm:text-sm"
                                placeholder="Enter Name"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.due_date}
                            />
                            {formik.touched.due_date && formik.errors.due_date ? (
                                <div className="text-red-400 font-size text-sm py-1">{formik.errors.due_date}</div>
                            ) : null}

                        </div>
                    </div>

                    <div>
                        <label htmlFor="HeadlineAct" className="block text-sm font-medium text-gray-900"> Status </label>

                        <select
                            name="status"
                            id="HeadlineAct"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.status}
                            className="mt-1.5 w-full rounded-lg border-gray-300 text-gray-700 sm:text-sm"
                        >
                            <option value="">Please select</option>
                            <option value="to-do">To Do</option>
                            <option value="in-progress">In Progress</option>
                            <option value="done">Done</option>

                        </select>

                        {formik.touched.status && formik.errors.status ? (
                            <div className="text-red-400 font-size text-sm py-1">{formik.errors.status}</div>
                        ) : null}
                    </div>


                    <button
                        type="submit"
                        className="block w-full rounded-lg bg-indigo-600 px-5 py-3 text-sm font-medium text-white"
                    >
                        {data.length > 0 ? 'Edit' : 'Add'}
                    </button>


                </form>
            </div>
        </div>
    </>
}
export default AddTask