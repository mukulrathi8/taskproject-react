import axios from "axios";
import TokenService from "./token.service";
import {API_URL} from "../constant"

export const AxiosInstance = axios.create({
    baseURL: API_URL,
    //timeout: 150000,
    headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
    },
    withCredentials: false,
});

AxiosInstance.interceptors.request.use(
    (config) => {
        const token = TokenService.getLocalAccessToken();
        if (token) {
            config.headers["Authorization"] = 'Bearer ' + token; 
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

export default AxiosInstance
