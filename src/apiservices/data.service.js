import { API_URL } from '../constant.jsx';
import {AxiosInstance as api } from './api.js';

class DataService {

    async signIn(obj = {}) {
        return api.post(API_URL+"sign-in",obj);
    }

    async createAccount(obj = {}) {
        return api.post(API_URL+"create-account",obj);
    }

    async addProject(obj = {}) {
        return api.post(API_URL+"create-project",obj);
    }

    async listProject(rowGuid=null) {
        const url =  rowGuid ? `${API_URL}list-project/${rowGuid}` : `${API_URL}list-project`;
        return api.get(url);
    }


    async viewProjectData(rowGuid) {
        const url = `${API_URL}list-project-task/${rowGuid}`;
        return api.get(url);
    }

    async updateProject(rowGuid,obj = {}) {
        return api.patch(API_URL + `update/${rowGuid}`,obj);
    }

    async addTask(obj = {}) {
        return api.post(API_URL + 'add-task',obj);
    }

}

export default new DataService();